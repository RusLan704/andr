package com.example.myapplication.callback;

public interface ResponseCallback {
    void response();
    void progressBar(boolean visible);
    void deserializationJson();
}
