package com.example.myapplication.Main;

import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

public interface InterfacePresenterFotMainActivity {

    void CreateRecycleViewList(RecyclerView numbersList);
    void filter(String text);
    void getIntentField();
}
