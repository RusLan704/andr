package com.example.myapplication.Main;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

public class ListView extends AppCompatActivity implements InterfaceForMainActivity {

    private RecyclerView numbersList;

    private PresenterForMainActivity presenterForMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenterForMainActivity = new PresenterForMainActivity(this);
        presenterForMainActivity.getIntentField();
        numbersList = findViewById(R.id.rv_numbers);
        presenterForMainActivity.CreateRecycleViewList(numbersList);
        Search();
    }

    @Override
    public void Search() {
        EditText search = findViewById(R.id.et_search);
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                presenterForMainActivity.filter(s.toString());
            }
        });
    }

}
