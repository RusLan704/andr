package com.example.myapplication.Main;



import android.content.Intent;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Currency;
import com.example.myapplication.Calcul.ModelCalculator;
import com.example.myapplication.NumbersAdapter;

import java.util.ArrayList;
import java.util.List;


public class PresenterForMainActivity implements InterfacePresenterFotMainActivity {

    private NumbersAdapter numbersAdapter;
    ListView mainActivity;
    ModelCalculator model;
    List<Currency> countryRecyclerListList;

    public PresenterForMainActivity(ListView mainActivity)
   {
        this.mainActivity=mainActivity;
        model = new ModelCalculator();
   }

    @Override
    public void CreateRecycleViewList(RecyclerView numbersList){
        LinearLayoutManager layoutManager = new LinearLayoutManager(mainActivity);
        numbersList.setLayoutManager(layoutManager);
        numbersList.setHasFixedSize(true);
        numbersAdapter = new NumbersAdapter(countryRecyclerListList, mainActivity);
        numbersList.setAdapter(numbersAdapter);
    }

    @Override
    public void filter(String text){
        ArrayList<Currency> search = new ArrayList<>();
        for(Currency item: countryRecyclerListList)
            if(item.getCountry().toLowerCase().contains(text.toLowerCase())){
                search.add(item);
            }
        numbersAdapter.filterlist(search);
    }

    @Override
    public void getIntentField(){
        Intent calculIntent = mainActivity.getIntent();
        if(calculIntent.hasExtra("List")) {
            countryRecyclerListList = (List<Currency>)calculIntent.getSerializableExtra("List");
        }
    }
}
