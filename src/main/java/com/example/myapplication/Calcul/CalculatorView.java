package com.example.myapplication.Calcul;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.view.View.OnClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.myapplication.R;


public class CalculatorView extends AppCompatActivity implements ICalculatorView {

    PresenterCalculator presCalcul;

    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button buttonZero;
    Button equality;
    Button comma;
    Button minus;
    Button plus;
    Button multiplication;
    Button division;
    Button C;
    ImageButton updateCurrency;

    EditText input;

    TextView output;
    TextView message;

    Button swapCurrencies;
    Button behind;
    ImageButton theFirstCurrency;
    ImageButton secondCurrency;

    ProgressBar progressBar;

    SharedPreferences myPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcul);

        presCalcul = new PresenterCalculator(this);

        input = findViewById(R.id.et_inputText);
        output = findViewById(R.id.outoutText);
        message = findViewById(R.id.messege);
        theFirstCurrency = findViewById(R.id.imageButton2);
        secondCurrency = findViewById(R.id.imageButton3);
        updateCurrency = findViewById(R.id.updateCurrency);

        button1 = findViewById(R.id.buttonOne);
        button2 = findViewById(R.id.buttonTwo);
        button3 = findViewById(R.id.buttonThree);
        button4 = findViewById(R.id.buttonFour);
        button5 = findViewById(R.id.buttonFive);
        button6 = findViewById(R.id.buttonSix);
        button7 = findViewById(R.id.buttonSeven);
        button8 = findViewById(R.id.buttonEight);
        button9 = findViewById(R.id.buttonNine);
        buttonZero = findViewById(R.id.buttonZer);
        equality = findViewById(R.id.buttonEquals);
        comma = findViewById(R.id.buttonComma);
        minus = findViewById(R.id.buttonMinus);
        plus = findViewById(R.id.buttonPlus);
        multiplication = findViewById(R.id.buttonMulriplacation);
        division = findViewById(R.id.buttonDevision);
        C = findViewById(R.id.buttonC);
        behind = findViewById(R.id.imageButtonBehind);
        swapCurrencies = findViewById(R.id.swap);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(ProgressBar.INVISIBLE);

        presCalcul.init();

        myPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        presCalcul.searchForAMultiplier();

        input.setInputType(InputType.TYPE_NULL);

        OnClickListener oMyButton = new OnClickListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.buttonOne:
                        presCalcul.buttonClick(1);
                        break;
                    case R.id.buttonTwo:
                        presCalcul.buttonClick(2);
                        break;
                    case R.id.buttonThree:
                        presCalcul.buttonClick(3);
                        break;
                    case R.id.buttonFour:
                        presCalcul.buttonClick(4);
                        break;
                    case R.id.buttonFive:
                        presCalcul.buttonClick(5);
                        break;
                    case R.id.buttonSix:
                        presCalcul.buttonClick(6);
                        break;
                    case R.id.buttonSeven:
                        presCalcul.buttonClick(7);
                        break;
                    case R.id.buttonEight:
                        presCalcul.buttonClick(8);
                        break;
                    case R.id.buttonNine:
                        presCalcul.buttonClick(9);
                        break;
                    case R.id.buttonEquals:
                        presCalcul.equals();
                        break;
                    case R.id.buttonComma:
                        presCalcul.comma();
                        break;
                    case R.id.buttonMinus:
                        presCalcul.operation("-");
                        break;
                    case R.id.buttonPlus:
                        presCalcul.operation("+");
                        break;
                    case R.id.buttonMulriplacation:
                        presCalcul.operation("*");
                        break;
                    case R.id.buttonDevision:
                        presCalcul.operation("/");
                        break;
                    case R.id.buttonC:
                        presCalcul.clear();
                        break;
                    case R.id.buttonZer:
                        presCalcul.setZero();
                        break;
                    case R.id.imageButtonBehind:
                        presCalcul.removeTheLastDigit();
                        break;
                    case R.id.imageButton2:
                        presCalcul.clickImageButton(1);
                        break;
                    case R.id.imageButton3:
                        presCalcul.clickImageButton(2);
                        break;
                    case R.id.swap:
                        presCalcul.swapCurrencies();
                        break;
                    case R.id.updateCurrency:
                        presCalcul.updateCurrency();
                        break;
                }
            }
        };

        button1.setOnClickListener(oMyButton);
        button2.setOnClickListener(oMyButton);
        button3.setOnClickListener(oMyButton);
        button4.setOnClickListener(oMyButton);
        button5.setOnClickListener(oMyButton);
        button6.setOnClickListener(oMyButton);
        button7.setOnClickListener(oMyButton);
        button8.setOnClickListener(oMyButton);
        button9.setOnClickListener(oMyButton);
        buttonZero.setOnClickListener(oMyButton);
        equality.setOnClickListener(oMyButton);
        comma.setOnClickListener(oMyButton);
        minus.setOnClickListener(oMyButton);
        plus.setOnClickListener(oMyButton);
        multiplication.setOnClickListener(oMyButton);
        division.setOnClickListener(oMyButton);
        C.setOnClickListener(oMyButton);
        behind.setOnClickListener(oMyButton);
        theFirstCurrency.setOnClickListener(oMyButton);
        secondCurrency.setOnClickListener(oMyButton);
        swapCurrencies.setOnClickListener(oMyButton);
        updateCurrency.setOnClickListener(oMyButton);
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences.Editor editor = myPreferences.edit();
        editor.putString("InputField", getInputField());
        editor.putString("OutputField", output.getText().toString());
        editor.putInt("numberOfTheCurrencyFieldThatWasClicked", presCalcul.getNumberOfTheCurrencyFieldThatWasClicked());
        editor.putInt("IndexObj1", presCalcul.getIndexObj1());
        editor.putInt("IndexObj2", presCalcul.getIndexObj2());
        editor.putString("JSONListOfCurrency", presCalcul.getJsonList());
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (myPreferences.contains("InputField")) {
            setInput(myPreferences.getString("InputField", null));
            showRes(myPreferences.getString("OutputField", null));
            presCalcul.clear();
            presCalcul.setNumberOfTheCurrencyFieldThatWasClicked(myPreferences.getInt("numberOfTheCurrencyFieldThatWasClicked", 0));
            presCalcul.setIndexObj1(myPreferences.getInt("IndexObj1", 0));
            presCalcul.setIndexObj2(myPreferences.getInt("IndexObj2", 0));
            presCalcul.getIntentMainActivity();
            presCalcul.setPhotoInImageButton();
            presCalcul.messege();
            presCalcul.setJsonList(myPreferences.getString("JSONListOfCurrency", null));
            presCalcul.deserializationJson();
            presCalcul.updateCurrency();
        }
    }

    @Override
    public void setMessage(String cur1, String cur2, String value) {
        message.setText("1 " + cur1 + " = " + value + " " + cur2);
    }

    @Override
    public void showRes(String res) {
        output.setText(res);
    }

    @Override
    public void setInput(String res) {
        input.setText(res);
    }

    @Override
    public String getInputField() {
        String data = input.getText().toString();
        return data;
    }

    @Override
    public void clearInputField() {
        input.setText("");
    }

    @Override
    public void clearOutputField() {
        output.setText("0");
    }

    @Override
    public void setPhotoForImageButton(int numberCurrency, int photo) {
        if (numberCurrency == 1)
            theFirstCurrency.setImageResource(photo);
        else secondCurrency.setImageResource(photo);
    }

}
