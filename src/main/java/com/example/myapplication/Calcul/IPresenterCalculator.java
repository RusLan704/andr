package com.example.myapplication.Calcul;


import java.math.BigDecimal;
import java.text.DecimalFormat;

public interface IPresenterCalculator {

    void buttonClick(double button);
    void comma();
    void operation(String sig);
    void showInputAndResult(BigDecimal value);
    void equals();
    void clear();
    void removeTheLastDigit();
    void setZero();
    void init();
    void updateCurrency();
    void setPhotoInImageButton();
    void setResult(BigDecimal value, DecimalFormat df);
    void clickImageButton(int currencyNumber);
    void searchForAMultiplier();
    void getIntentMainActivity();
    int getNumberOfTheCurrencyFieldThatWasClicked();
    void setNumberOfTheCurrencyFieldThatWasClicked(int s);
    int getIndexObj1();
    void setIndexObj1(int indexObj1);
    int getIndexObj2();
    void setIndexObj2(int indexObj2);

}
