package com.example.myapplication.Calcul;

import com.example.myapplication.callback.ResponseCallback;

import org.json.JSONException;

public interface IAppApiHelper {
    void gettingAMonetaryUnitOfCurrency(ResponseCallback callback);
    void jsonProcessing( String listOfCurrencies) throws JSONException;
}
