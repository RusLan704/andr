package com.example.myapplication.Calcul;


import com.example.myapplication.Currency;
import com.example.myapplication.R;
import com.example.myapplication.callback.ResponseCallback;

import java.util.ArrayList;
import java.util.List;

public class ModelCalculator implements IModelCalсulator {

   private  List<Currency> countryRecyclerListList = new ArrayList<>();
    AppApiHelper appApiHelper;

    public ModelCalculator(){
        appApiHelper = new AppApiHelper(countryRecyclerListList);
    }

    @Override
    public Currency getAListObject(int index){
        return countryRecyclerListList.get(index);
    }

    @Override
    public List<Currency> getList (){
        return countryRecyclerListList;
    }
    public void setCountryRecyclerListList(List<Currency> list){
        countryRecyclerListList = list;
    }

    @Override
    public void initializateData() {
            countryRecyclerListList.add(new Currency("Российский рубль", "RUB", R.drawable.russia_round_icon_64));
            countryRecyclerListList.add(new Currency("Доллар США", "USD", R.drawable.united_states_of_america_round_icon_64));
            countryRecyclerListList.add(new Currency("Евро", "EUR", R.drawable.european_union_round_icon_64));
            countryRecyclerListList.add(new Currency("Британский фунт", "GBP", R.drawable.united_kingdom_round_icon_64));
            countryRecyclerListList.add(new Currency("Швейцарский франк", "CHF", R.drawable.switzerland_round_icon_64));
            countryRecyclerListList.add(new Currency("Чешская крона", "CZK", R.drawable.czech_republic_round_icon_64));
            countryRecyclerListList.add(new Currency("Белорусский рубль", "BYN", R.drawable.belarus_round_icon_64));
            countryRecyclerListList.add(new Currency("Азербайджанский манат", "AZN", R.drawable.azerbaijan_round_icon_64));
            countryRecyclerListList.add(new Currency("Украинская гривна", "UAH", R.drawable.ukraine_round_icon_64));
            countryRecyclerListList.add(new Currency("Бразильский реал", "BRL", R.drawable.brazil_round_icon_64));
            countryRecyclerListList.add(new Currency("Болгарский лев", "BGN", R.drawable.bulgaria_round_icon_64));
            countryRecyclerListList.add(new Currency("Канадский доллар", "CAD", R.drawable.canada_round_icon_64));
            countryRecyclerListList.add(new Currency("Китайский юань", "CNY", R.drawable.china_round_icon_64));
            countryRecyclerListList.add(new Currency("Польский злотый", "PLN", R.drawable.poland_round_icon_64));
            countryRecyclerListList.add(new Currency("Датская крона", "DKK", R.drawable.denmark_round_icon_64));
            countryRecyclerListList.add(new Currency("Гонконгский доллар", "HKD", R.drawable.hong_kong_round_icon_64));
            countryRecyclerListList.add(new Currency("Венгерский форинт", "HUF", R.drawable.hungary_round_icon_64));
            countryRecyclerListList.add(new Currency("Индийская рупия", "INR", R.drawable.india_round_icon_64));
            countryRecyclerListList.add(new Currency("Казахский тенге", "KZT", R.drawable.kazakhstan_round_icon_64));
            countryRecyclerListList.add(new Currency("Японский иен", "JPY", R.drawable.japan_round_icon_64));
            countryRecyclerListList.add(new Currency("Киргизский сом", "KGS", R.drawable.kyrgyzstan_round_icon_64));
    }

    @Override
    public void currencySearch(ResponseCallback callback){
        appApiHelper.gettingAMonetaryUnitOfCurrency(callback);
    }

    public int searchIndex(String ABR) {
        int index=0;
        for (int i = 0; i < countryRecyclerListList.size(); i++) {
            if (countryRecyclerListList.get(i).getCurrency().equals(ABR))
                index = i;
        }
        return index;
    }
}




