package com.example.myapplication.Calcul;

import com.example.myapplication.Currency;
import com.example.myapplication.callback.ResponseCallback;

import java.util.List;

public interface IModelCalсulator {

    Currency getAListObject(int index);

    List<Currency> getList();

    void initializateData();

    void currencySearch(ResponseCallback callback);

}