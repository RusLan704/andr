package com.example.myapplication.Calcul;

import android.content.Intent;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.myapplication.Currency;
import com.example.myapplication.Main.ListView;
import com.example.myapplication.NetworkUnits;
import com.example.myapplication.callback.ResponseCallback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;


public class PresenterCalculator implements IPresenterCalculator, ResponseCallback {

    private CalculatorView calcul;
    private ModelCalculator model;

    private int indexObj1 = 0;
    private int indexObj2 = 1;

    private BigDecimal operand1;
    private String sign;
    private Boolean operationWasClicked = false;
    private int numberOfTheCurrencyFieldThatWasClicked;
    private double multiplier;

    private String jsonList;

    public PresenterCalculator(CalculatorView calcul) {
        this.calcul = calcul;
        model = new ModelCalculator();
    }

    @Override
    public void init() {
        model.initializateData();
        setPhotoInImageButton();
        updateCurrency();
    }

    @Override
    public void updateCurrency() {
        if (NetworkUnits.isOnline(calcul)) {
            model.currencySearch(this);
        }
    }

    @Override
    public void setPhotoInImageButton() {
        calcul.setPhotoForImageButton(1, model.getAListObject(indexObj1).getPhotoId());
        calcul.setPhotoForImageButton(2, model.getAListObject(indexObj2).getPhotoId());
    }

    @Override
    public void buttonClick(double button) {
        if (operationWasClicked) {
            calcul.clearInputField();
            operationWasClicked = false;
        }

        showInputAndResult(new BigDecimal(button));
    }

    @Override
    public void showInputAndResult(BigDecimal value) {
        if (calcul.getInputField().length() < 16) {
            DecimalFormat df = new DecimalFormat();
            df.setGroupingUsed(true);
            df.setGroupingSize(3);
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator('.');
            decimalFormatSymbols.setGroupingSeparator(' ');
            df.setDecimalFormatSymbols(decimalFormatSymbols);

            calcul.input.append(String.valueOf(value));

            BigDecimal input = new BigDecimal(getInputFieldWithoutASpace());

            if (input.scale() > 3) {
                BigDecimal integerPart = input.setScale(0, RoundingMode.DOWN);
                calcul.setInput(df.format(integerPart));
                BigInteger drob = input.subtract(input.setScale(0, RoundingMode.DOWN)).unscaledValue();
                calcul.input.append("." + drob);

            } else {
                calcul.setInput(df.format(input));
            }

            BigDecimal result = input;

            if (!Double.isNaN(multiplier))
                result = result.multiply(BigDecimal.valueOf(multiplier).setScale(3, BigDecimal.ROUND_HALF_DOWN));

            setResult(result, df);
        }
    }

    @Override
    public void setResult(BigDecimal value, DecimalFormat df) {

        String result = df.format(value);

        if (result.length() < 26) {
            calcul.showRes(result);
        }
    }


    @Override
    public void comma() {

        String str = calcul.getInputField();
        if ("".equals(str))
            return;
        else if (!str.contains("."))
            calcul.input.append(".");
    }

    @Override
    public void setZero() {
        if (calcul.getInputField().length() > 0)
            buttonClick(0);
    }

    public String getInputFieldWithoutASpace() {
        return calcul.getInputField().replaceAll(" ", "");
    }

    @Override
    public void operation(String sig) {
        if ("".equals(calcul.getInputField()))
            return;

        if (sign == null) {
            operand1 = new BigDecimal(getInputFieldWithoutASpace());
            sign = sig;
            operationWasClicked = true;

        } else if (!operationWasClicked) {

            BigDecimal operand2 = new BigDecimal(getInputFieldWithoutASpace());
            calcul.clearInputField();

            switch (sign) {
                case "-":
                    operand1 = operand1.subtract(operand2);
                    showInputAndResult(operand1);
                    break;
                case "+":
                    operand1 = operand1.add(operand2);
                    showInputAndResult(operand1);
                    break;
                case "*":
                    operand1 = operand1.multiply(operand2);
                    showInputAndResult(operand1);
                    break;
                case "/":
                    operand1 = operand1.divide(operand2, 2, RoundingMode.HALF_DOWN);
                    showInputAndResult(operand1);
                    break;
            }
            operationWasClicked = true;
        }
        sign = sig;
    }

    @Override
    public void equals() {
        operation("=");
        operand1 = new BigDecimal(0);
        operationWasClicked = true;
        sign = null;
    }

    @Override
    public void clear() {
        sign = null;
        operand1 = new BigDecimal(0);
        operationWasClicked = true;
        calcul.clearInputField();
        calcul.clearOutputField();
    }

    @Override
    public void removeTheLastDigit() {
        String inputField = getInputFieldWithoutASpace();
        if (!"".equals(inputField) && inputField.length() != 1) {
            String inputField1 = inputField.substring(0, inputField.length() - 1);
            calcul.clearInputField();
            showInputAndResult(new BigDecimal(inputField1.replaceAll(" ", "")));
        } else {
            calcul.clearInputField();
            calcul.clearOutputField();
        }
    }

    @Override
    public void clickImageButton(int currencyNumber) {
        numberOfTheCurrencyFieldThatWasClicked = currencyNumber;
        Class destinationActivity = ListView.class;

        Intent mainActivityIntent = new Intent(calcul, destinationActivity);
        List<Currency> list = model.getList();
        mainActivityIntent.putExtra("List", (Serializable) list);
        calcul.startActivity(mainActivityIntent);
    }

    public void swapCurrencies() {

        int tmp = indexObj1;
        indexObj1 = indexObj2;
        indexObj2 = tmp;
        searchForAMultiplier();
        setPhotoInImageButton();
        String resInp = getInputFieldWithoutASpace();
        messege();
        if (!resInp.equals("")) {
            clear();
            operationWasClicked = false;
            showInputAndResult(new BigDecimal(resInp));
        }
    }

    @Override
    public void getIntentMainActivity() {
        Intent calculIntent = calcul.getIntent();
        if (calculIntent.hasExtra("objIntent")) {
            String abr = calculIntent.getStringExtra("objIntent");
            int indexOfTheListFromIntent = model.searchIndex(abr);

            if (indexOfTheListFromIntent != indexObj1 && indexOfTheListFromIntent != indexObj2) {
                calcul.setPhotoForImageButton(numberOfTheCurrencyFieldThatWasClicked,
                        model.getAListObject(indexOfTheListFromIntent).getPhotoId());
                if (numberOfTheCurrencyFieldThatWasClicked == 1)
                    indexObj1 = indexOfTheListFromIntent;
                else indexObj2 = indexOfTheListFromIntent;
            }
        }
    }

    public void messege() {
        String cur1 = model.getAListObject(indexObj1).getCurrency();
        String cur2 = model.getAListObject(indexObj2).getCurrency();
        String value = String.format("%.3f", multiplier);

        if (!Double.isNaN(multiplier))
            calcul.setMessage(cur1, cur2, value);
        else
            calcul.setMessage(cur1, cur2, "NaN");
    }

    @Override
    public void response() {
        searchForAMultiplier();
        messege();
    }

    public String getJsonList() {
        List<Currency> list = model.getList();
        String jsonList = new Gson().toJson(list);
        return jsonList;

    }

    @Override
    public void deserializationJson() {
        Type type = new TypeToken<List<Currency>>() {
        }.getType();
        List<Currency> list = new Gson().fromJson(jsonList, type);
        model.setCountryRecyclerListList(list);
        searchForAMultiplier();
        messege();
    }


    @Override
    public void progressBar(boolean visible) {
        if (visible) {
            calcul.updateCurrency.setVisibility(ImageButton.INVISIBLE);
            calcul.progressBar.setVisibility(ProgressBar.VISIBLE);
        } else {
            calcul.progressBar.setVisibility(ProgressBar.INVISIBLE);
            calcul.updateCurrency.setVisibility(ImageButton.VISIBLE);
        }
    }


    @Override
    public void searchForAMultiplier() {
        double divident = model.getAListObject(indexObj1).getPar();
        double diviler = model.getAListObject(indexObj2).getPar();
        multiplier = divident / diviler;
    }

    @Override
    public int getNumberOfTheCurrencyFieldThatWasClicked() {
        return numberOfTheCurrencyFieldThatWasClicked;
    }

    @Override
    public void setNumberOfTheCurrencyFieldThatWasClicked(int s) {
        numberOfTheCurrencyFieldThatWasClicked = s;
    }

    @Override
    public int getIndexObj1() {
        return indexObj1;
    }

    @Override
    public void setIndexObj1(int indexObj1) {
        this.indexObj1 = indexObj1;
    }

    @Override
    public int getIndexObj2() {
        return indexObj2;
    }

    @Override
    public void setIndexObj2(int indexObj2) {
        this.indexObj2 = indexObj2;
    }

    public void setJsonList(String jsonList) {
        this.jsonList = jsonList;
    }

}
