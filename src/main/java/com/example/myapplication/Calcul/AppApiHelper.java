package com.example.myapplication.Calcul;

import android.os.AsyncTask;

import com.example.myapplication.Currency;
import com.example.myapplication.NetworkUnits;
import com.example.myapplication.callback.ResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class AppApiHelper implements IAppApiHelper {

    private List<Currency> countryRecyclerListList;

    public AppApiHelper(List<Currency> countryRecyclerListList) {
        this.countryRecyclerListList = countryRecyclerListList;
    }

    @Override
    public void gettingAMonetaryUnitOfCurrency(ResponseCallback callback) {
        URL url = NetworkUnits.generateURL();
        new NewFlow(callback).execute(url);
    }

    @Override
    public void jsonProcessing(String listOfCurrencies) throws JSONException {
        if (listOfCurrencies != null) {
            double resultValue;
            JSONObject jsonResponse = new JSONObject(listOfCurrencies);
            JSONObject jsonObject = jsonResponse.getJSONObject("Valute");
            countryRecyclerListList.get(0).setPar(1);

            for (int i = 1; i < countryRecyclerListList.size(); i++) {
                JSONObject state = jsonObject.getJSONObject(countryRecyclerListList.get(i).getCurrency());

                resultValue = state.getDouble("Value");
                countryRecyclerListList.get(i).setPar(resultValue);
            }
        }
    }

    class NewFlow extends AsyncTask<URL, Void, String> {
        private ResponseCallback callback;
        private String listOfCurrencies;

        public NewFlow(ResponseCallback callback) {
            this.callback = callback;
        }

        @Override
        protected void onPreExecute() {
            callback.progressBar(true);
        }

        @Override
        protected String doInBackground(URL... urls) {
            try {
                listOfCurrencies = NetworkUnits.getResponseFromURL(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return listOfCurrencies;
        }

        @Override
        protected void onPostExecute(String listOfCurrencies) {
            try {
                jsonProcessing(listOfCurrencies);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            callback.response();
            callback.progressBar(false);

        }
    }
}
