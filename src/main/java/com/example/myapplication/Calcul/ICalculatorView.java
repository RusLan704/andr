package com.example.myapplication.Calcul;

public interface ICalculatorView {
    void showRes(String res);
    void setInput(String res);
    String getInputField();
    void clearInputField();
    void clearOutputField();
    void setPhotoForImageButton(int numberCurrency, int photo);
    void setMessage(String cur1, String cur2, String value);
}
