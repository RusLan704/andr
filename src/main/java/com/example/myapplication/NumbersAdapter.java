package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Calcul.CalculatorView;

import java.util.ArrayList;
import java.util.List;

public class NumbersAdapter extends RecyclerView.Adapter<NumbersAdapter.NumberViewHolder> {

    private List<Currency> countryRecyclerLists;
    private Context context;

    public NumbersAdapter(List<Currency> countryRecyclerLists, Context context) {
        this.countryRecyclerLists = countryRecyclerLists;
        this.context = context;
    }

    @NonNull
    @Override
    public NumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListenItem = R.layout.number_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListenItem, parent, false);
        NumberViewHolder viewHolder = new NumberViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NumberViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return countryRecyclerLists.size();
    }

    public void filterlist(ArrayList<Currency> t) {
        countryRecyclerLists = t;
        notifyDataSetChanged();
    }

    class NumberViewHolder extends RecyclerView.ViewHolder {

        TextView currencyName;
        TextView currencyAbbreviation;
        ImageView countryPhoto;

        public String getCab() {
            return currencyAbbreviation.getText().toString();
        }

        public NumberViewHolder(@NonNull final View itemView) {
            super(itemView);
            currencyName = itemView.findViewById(R.id.tv_number_item);
            currencyAbbreviation = itemView.findViewById(R.id.tw_view_holder_number);
            countryPhoto = itemView.findViewById(R.id.iv_imagesForRecycleView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String ABR = getCab();
                    final Class destinationActivity = CalculatorView.class;
                    final Intent calculIntent = new Intent(context, destinationActivity);
                    calculIntent.putExtra("objIntent", ABR);
                    context.startActivity(calculIntent);
                }
            });
        }

        void bind(int listIndex) {
            currencyName.setText(countryRecyclerLists.get(listIndex).getCountry());
            currencyAbbreviation.setText(countryRecyclerLists.get(listIndex).getCurrency());
            countryPhoto.setImageResource(countryRecyclerLists.get(listIndex).getPhotoId());
            currencyName.setTextColor(Color.WHITE);
            currencyAbbreviation.setTextColor(Color.WHITE);
        }
    }

}