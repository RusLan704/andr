package com.example.myapplication;

import java.io.Serializable;

public class Currency implements Serializable {

    private String country;
    private String currency;
    private int photoid;
    private double par;

    public Currency(String country, String currency, int photoid) {
        this.country = country;
        this.currency = currency;
        this.photoid = photoid;
    }

    public String getCountry() {
        return country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setPar(double par) {
        this.par = par;
    }

    public int getPhotoId() {
        return photoid;
    }

    public double getPar() {
        return par;
    }

}
